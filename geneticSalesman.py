from   gsm    import *
import random
import math
import sys

#config
populationNumber = 50
epochNumber      = 100

#data input
data = list()
if len(sys.argv) == 1: #no data input, default values
    data = (
        (60,  200), (180, 200), (80,  180), (140, 180), 
        (20,  160), (100, 160), (200, 160), (140, 140),
        (40,  120), (100, 120), (180, 100), (60,   80),
        (120,  80), (180,  60), (20,   40), (100,  40),
        (200,  40), (20,   20), (60,   20), (160,  20)
    )
else: #input data directly in CLI arguments
    for x in range(1, len(sys.argv)):
        data.append([int(n) for n in sys.argv[x].split(',')])

#network initialisation
network = Network()
for x in xrange(len(data)):
    network.appendNode(data[x][0], data[x][1])

#population initialisation
population = Population(populationNumber, network, True)

#evolving...
evolution = Evolution(network) 
population = evolution.evolvePopulation(population)
for x in xrange(epochNumber):
	population = evolution.evolvePopulation(population)
	
#DONE! output result
for x in xrange(len(data)):
    print(population.getFittestRoute().getRoutePosition(x).getInformations()),