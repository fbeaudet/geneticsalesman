from node import Node

class Network:
    
    nodes = list()

    def appendNode(self, x, y):
        self.nodes.append(Node(x, y))
        return True

    def getNodeCount(self):
        return len(self.nodes)

    def getNode(self, nodeId):
        return self.nodes[nodeId]