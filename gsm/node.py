import math

class Node:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def getDistance(self, node):
        	xDist = abs(self.x - node.x)
        	yDist = abs(self.y - node.y)
        	return math.sqrt(xDist * xDist + yDist * yDist)

    def getInformations(self):
    	return str(self.x) +','+ str(self.y)