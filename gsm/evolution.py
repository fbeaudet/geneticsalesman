from   population import Population
from   route      import Route
import random

class Evolution:

    mutationRate = 0.015 #between 0-1
    tournamentSize = 5 #number of routes
    elitism = True

    def __init__(self, network):
        self.network = network

    def _crossOver(self, parentA, parentB):
        child = Route(self.network)
        child.generateRandomRoute
        startPos = int(random.random() * parentA.getPositionCount());
        endPos = int(random.random() * parentA.getPositionCount());
        #parentA
        for x in xrange(child.getPositionCount()):
            if startPos < endPos and x > startPos and x < endPos:
                child.pushRoutePosition(x, parentA.getRoutePosition(x))
            elif startPos > endPos:
                if (x < startPos and x > endPos) == False:
                    child.pushRoutePosition(x, parentA.getRoutePosition(x))
        #parentB
        for x in xrange(parentB.getPositionCount()):
            if child.containsNode(parentB.getRoutePosition(x)) == False:
                for y in xrange(child.getPositionCount()):
                    if child.getRoutePosition(y) == None:
                        child.pushRoutePosition(y, parentB.getRoutePosition(x))
                        break
        return child

    def _mutateRoute(self, route):
        for x in xrange(route.getPositionCount()):
            if random.random() < self.mutationRate:
                y = int(random.random() * route.getPositionCount())
                nodeA = route.getRoutePosition(x)
                nodeB = route.getRoutePosition(y)
                route.pushRoutePosition(y, nodeA)
                route.pushRoutePosition(x, nodeB)

    def _selectRoute(self, population):
        selection = Population(self.tournamentSize, population.network, False)
        for x in xrange(self.tournamentSize):
            randomId = int(random.random() * population.getRouteCount())
            selection.pushRoute(x, population.getRoute(randomId))
        return selection.getFittestRoute()


    def evolvePopulation(self, population):
        evolvedPopulation = Population(population.getRouteCount(), population.network, False)
        eliteOffset = 0
        if self.elitism:
            evolvedPopulation.pushRoute(0, population.getFittestRoute())
            eliteOffset = 1
        for x in range(eliteOffset, evolvedPopulation.getRouteCount()):
            parentA = self._selectRoute(population)
            parentB = self._selectRoute(population)
            child = self._crossOver(parentA, parentB)
            evolvedPopulation.pushRoute(x, child)
        for x in range(eliteOffset, evolvedPopulation.getRouteCount()):
            self._mutateRoute(evolvedPopulation.getRoute(x))
        return evolvedPopulation