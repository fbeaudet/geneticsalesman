import random

class Route:

    fitness = 0
    distance = 0
    positions = list()

    def __init__(self, network):
        self.network = network
        self.positions = [None] * self.network.getNodeCount()

    def generateRandomRoute(self):
        for x in xrange(self.network.getNodeCount()):
            self.pushRoutePosition(x, self.network.getNode(x))
        random.shuffle(self.positions)

    def getPositionCount(self):
        return len(self.positions)

    def getDistance(self):
        if self.distance == 0:
            routeDistance =  0
            for x in xrange(self.getPositionCount()):
                nodeA = self.getRoutePosition(x)
                if nodeA == None:
                    break
                if x+1 < self.getPositionCount():
                    nodeB = self.getRoutePosition(x+1)
                else:
                    nodeB = self.getRoutePosition(0)
                if nodeB == None:
                    break
                routeDistance += nodeA.getDistance(nodeB)
            self.distance = routeDistance
        return self.distance

    def getFitness(self):
        if self.fitness == 0:
            self.fitness = 1 / self.getDistance();
        return self.fitness

    def getRoutePosition(self, positionId):
        return self.positions[positionId]

    def containsNode(self, node):
        return node in self.positions

    def pushRoutePosition(self, positionId, node):
        self.positions[positionId] = node