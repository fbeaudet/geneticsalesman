from route import Route

class Population:

    def __init__(self, populationSize, network, randomGenerated):
        self.routes = list()
        self.network = network
        self.routes = [None] * populationSize
        if randomGenerated:
            for x in xrange(populationSize):
                route = Route(network)
                route.generateRandomRoute()
                self.pushRoute(x, route)

    def getRouteCount(self):
        return len(self.routes)

    def getFittestRoute(self):
        fittest = self.getRoute(0)
        for x in range(1, self.getRouteCount()):
            if fittest.getFitness() <= self.getRoute(x).getFitness():
                fittest = self.getRoute(x)
        return fittest

    def getRoute(self,routeId):
        return self.routes[routeId]

    def pushRoute(self, routeId, route):
            self.routes[routeId] = route